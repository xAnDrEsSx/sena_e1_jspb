package com.example.demo.services;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.example.demo.models.AprendizModel;
import com.example.demo.repositories.AprendizRepository;

@Service
public class AprendizService {
	
	@Autowired
	AprendizRepository aprendizRepository;
	
	// Listar todos los aprendices
	public List<AprendizModel> getAprendices(){
		return (List<AprendizModel>) aprendizRepository.findAll();
	}
	
	// crear un Aprendiz
	public AprendizModel addAprendiz(AprendizModel aprendizModel) {

		return aprendizRepository.save(aprendizModel);
	}
	

}
