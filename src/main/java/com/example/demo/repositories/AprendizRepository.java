package com.example.demo.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import com.example.demo.models.AprendizModel;


@Repository
public interface AprendizRepository extends CrudRepository<AprendizModel,Long> {

	
}