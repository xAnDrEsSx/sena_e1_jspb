package com.example.demo.controllers;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.example.demo.models.AprendizModel;
import com.example.demo.services.AprendizService;

@RestController
@RequestMapping("/Aprendiz")
public class AprendizController {
	
	@Autowired
	AprendizService aprendizService;
	
	@GetMapping()
	public List<AprendizModel> listarAprendices(){
		return aprendizService.getAprendices();
	}

}